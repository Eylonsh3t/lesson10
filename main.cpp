#include "Customer.h"
#include <map>

void printMenu();
int getOption();
std::string get_Name();
void existUserMenu();

int main()
{
	std::map<std::string, Customer> abcCustomers;
	std::map<std::string, Customer>::iterator it;

	Item itemList[10] = {
		Item("Milk","00001",5.3),
		Item("Cookies","00002",12.6),
		Item("bread","00003",8.9),
		Item("chocolate","00004",7.0),
		Item("cheese","00005",15.3),
		Item("rice","00006",6.2),
		Item("fish", "00008", 31.65),
		Item("chicken","00007",25.99),
		Item("cucumber","00009",1.21),
		Item("tomato","00010",2.32) };
	
	std::string name;
	int itemNum = 0;
	int option = 0;
	bool userExist = false;

	do
	{
		if (!userExist) // if user trying to add items to exist user.
		{
			printMenu();
			option = getOption();
		}

		switch (option)
		{
		case 1:
		{
			if (!userExist)
			{
				name = get_Name();
				it = abcCustomers.find(name);
			}

			if (it == abcCustomers.end())
			{
				Customer cust = Customer(name);
				std::pair<std::string, Customer> customer(name, cust);
				abcCustomers.insert(customer);

				std::cout << "The items you can buy are: (0 to exit)" << std::endl;
				for (int i = 0; i < 10; i++)
				{
					std::cout << itemList[i].getSerialNumber() << ". ";
					std::cout << itemList[i].getName();
					std::cout << " Price: " << itemList[i].getUnitPrice() << std::endl;
				}
				do
				{
					std::cout << "What item would you like to buy? Input: " << std::endl;
					std::cin >> itemNum;
					if (itemNum != 0)
					{
						Item chosenItem = itemList[itemNum - 1];
						cust.addItem(chosenItem);
					}
				} while (itemNum != 0);
				abcCustomers[name] = cust;
			}
			else if (userExist) // if user trying to add items to exist user.
			{
				std::cout << "The items you can buy are: (0 to exit)" << std::endl;
				for (int i = 0; i < 10; i++)
				{
					std::cout << itemList[i].getSerialNumber() << ". ";
					std::cout << itemList[i].getName();
					std::cout << " Price: " << itemList[i].getUnitPrice() << std::endl;
				}
				do
				{
					std::cout << "What item would you like to buy? Input: " << std::endl;
					std::cin >> itemNum;
					if (itemNum != 0)
					{
						Item chosenItem = itemList[itemNum - 1];
						it->second.addItem(chosenItem);
					}
				} while (itemNum != 0);
				userExist = false;
			}
			else
			{
				std::cout << "The name is already exist!" << std::endl;
			}
			
			break;
		}

		case 2:
		{
			int userOption = 0;
			name = get_Name();
			it = abcCustomers.find(name);

			if (it != abcCustomers.end())
			{
				it->second.printItems();
				existUserMenu();
				userOption = getOption();
				
				switch (userOption)
				{

				case 1:
				{
					userExist = true;
					option = 1;
					break;
				}

				case 2:
				{
					int count = 0;
					do
					{
						std::cout << "Which item you would like to erase? if you don't want to push '0'." << std::endl;
						std::cin >> itemNum;
						if (itemNum)
						{
							Item chosenItem = itemList[itemNum - 1];
							bool isItemExist = it->second.findItem(chosenItem.getSerialNumber());
							if (isItemExist)
							{
								it->second.removeItem(chosenItem);
							}
							else
							{
								std::cout << "you don't have the item in your list of items." << std::endl;
							}
						}
					} while (itemNum != 0);
					break;
				}

				default:
					break;
				}
			}
			else
			{
				std::cout << "The name is not exist!" << std::endl;
			}
			break;
		}

		case 3:
		{
			//https://www.programiz.com/cpp-programming/examples/array-largest-element
			int i = 0;
			int size = 0;
			double *highestSum = new double[abcCustomers.size()];
			for (it = abcCustomers.begin(); it != abcCustomers.end(); ++it, i++) // enters the total sum for every user.
			{
				highestSum[i] = it->second.totalSum();
			}
			// Loop to store largest number to arr[0]
			for (i = 1; i < abcCustomers.size(); ++i)
			{
				if (highestSum[0] < highestSum[i])
				{
					highestSum[0] = highestSum[i];
					size = i;
				}
			}
			i = 0;
			for (it = abcCustomers.begin(); it != abcCustomers.end(); ++it, i++)
			{
				if (i == size)
				{
					std::cout << "The customer that paid the most is: " << it->first << std::endl;
					std::cout << "The total price: " << it->second.totalSum() << std::endl;
				}
			}

			delete[] highestSum;
			break;
		}
		
		default:
		{
			std::cout << "Good bye!" << std::endl;
			option = 4;
			break;
		}
		}
	} while (option != 4);

	return 0;
}

void printMenu()
{
	std::cout << "Welcome to MagshiMart!" << std::endl;
	std::cout << "1. to sign as customer and buy items." << std::endl;
	std::cout << "2. to uptade existing customer's items." << std::endl;
	std::cout << "3. to print the customer who pays the most." << std::endl;
	std::cout << "4. to exit." << std::endl;
}

int getOption()
{
	int option;
	std::cin >> option;
	return option;
}

std::string get_Name()
{
	std::string name;
	std::cout << "Please enter the customer name: ";
	std::cin >> name;
	return name;
}

void existUserMenu()
{
	std::cout << "1. Add items." << std::endl;
	std::cout << "2. Remove items." << std::endl;
	std::cout << "3. Back to menu." << std::endl;
}