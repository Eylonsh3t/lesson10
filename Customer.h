#pragma once
#include "Item.h"
#include <set>

class Customer
{
public:
	Customer(std::string name);
	Customer();
	~Customer();
	double totalSum() const;//returns the total sum for payment
	void addItem(Item item);//add item to the set
	void removeItem(Item item);//remove item from the set

	bool findItem(std::string num);
	void printItems();

	//get and set functions
	std::string get_Name() const;

private:
	std::string _name;
	std::set<Item> _items;


};
