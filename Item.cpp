#include "Item.h"

Item::Item(std::string name, std::string serialNumber, double unitPrice)
{
	this->_name = name;
	this->_serialNumber = serialNumber;
	this->_unitPrice = unitPrice;
	this->_count = 1;
}

Item::~Item()
{
}

double Item::totalPrice() const
{
	return double(this->_unitPrice * this->_count);
}

bool Item::operator<(const Item& other) const
{
	return this->_serialNumber < other._serialNumber;
}

bool Item::operator>(const Item& other) const
{
	return this->_serialNumber > other._serialNumber;
}

bool Item::operator==(const Item& other) const
{
	return  this->_serialNumber == other._serialNumber;;
}

std::string Item::getName() const
{
	return this->_name;
}

std::string Item::getSerialNumber() const
{
	return this->_serialNumber;
}

double Item::getUnitPrice() const
{
	return this->_unitPrice;
}

int Item::getCount() const
{
	return this->_count;
}

void Item::setCount(int count)
{
	this->_count = count;
}
