#include "Customer.h"

Customer::Customer(std::string name)
{
	this->_name = name;
}

Customer::Customer() // i did it because i had to if i would not do that there will be a bug in the update of the customer.
{
}

Customer::~Customer()
{
}

double Customer::totalSum() const
{
	double totalPrice = 0;
	std::set<Item>::iterator it;
	for (it = this->_items.begin(); it != this->_items.end(); ++it)
	{
		totalPrice += it->totalPrice();
	}
	return totalPrice;
}

void Customer::addItem(Item item)
{
	std::set<Item>::iterator it;
	it = this->_items.find(item);
	if (it == this->_items.end())
	{
		this->_items.insert(item);
	}
	else
	{
		int count = it->getCount() + 1;
		this->_items.erase(item);
		item.setCount(count);
		this->_items.insert(item);
	}
}

void Customer::removeItem(Item item)
{
	std::set<Item>::iterator it;
	it = this->_items.find(item);
	if (it->getCount() > 1)
	{
		int count = it->getCount() - 1;
		this->_items.erase(item);
		item.setCount(count);
		this->_items.insert(item);
	}
	else
	{
		this->_items.erase(item);
	}
}

bool Customer::findItem(std::string num)
{
	std::set<Item>::iterator it;
	for (it = this->_items.begin(); it != this->_items.end(); ++it)
	{
		if (it->getSerialNumber() == num)
		{
			return true;
		}
	}
	return false;
}

void Customer::printItems()
{
	std::set<Item>::iterator it;
	for (it = this->_items.begin(); it != this->_items.end(); ++it)
	{
		std::cout << it->getSerialNumber() << ". ";
		std::cout << it->getName();
		std::cout << " Price: " << it->getUnitPrice() << std::endl;
	}
}

std::string Customer::get_Name() const
{
	return this->_name;
}
